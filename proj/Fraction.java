package proj;

public class Fraction {

	private int numerateur;
	private int denominateur;

	private static final int UN = 1;
	private static final int ZERO = 0;


	public Fraction (int numerateur, int denominateur){
		this.numerateur = numerateur;
		this.denominateur = denominateur;

	}
	public Fraction (int numerateur){
		this.numerateur = numerateur;
		this.denominateur = 1;
	}
	public Fraction (){
		this.numerateur = 0;
		this.denominateur = 1;
	}

	public int getNumerateur (){
		return this.numerateur;
	}
	public int getDenominateur (){
		return this.denominateur;
	}

	public String toString (){
		return (this.numerateur + "/" + this.denominateur);
	}
}
